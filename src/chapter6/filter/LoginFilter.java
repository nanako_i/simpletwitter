package chapter6.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter6.beans.User;

@WebFilter("/*")
public class LoginFilter implements Filter {

	public static String INIT_PARAMETER_NAME_ENCODING = "encoding";

	public static String DEFAULT_ENCODING = "UTF-8";

	private String encoding;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		if (request.getCharacterEncoding() == null) {
			request.setCharacterEncoding(encoding);
		}

		String servletPath = ((HttpServletRequest) request).getServletPath();

		// Allow access to login functionality.
		if (servletPath.equals("/login.jsp") || servletPath.equals("/index.jsp")
				|| servletPath.equals("/top.jsp") || servletPath.equals("/signup.jsp") || servletPath.equals("/")) {
			chain.doFilter(request, response);
			return;
		} else {

			// All other functionality requires authentication.
			HttpSession session = ((HttpServletRequest) request).getSession();
			User user = (User) session.getAttribute("loginUser");

			if (user != null) {
				// User is logged in.
				chain.doFilter(request, response);
				return;
			}
			// Request is not authorized.
			((HttpServletResponse) response).sendRedirect("login");
		}
	}

	@Override
	public void init(FilterConfig config) {
		encoding = config.getInitParameter(INIT_PARAMETER_NAME_ENCODING);
		if (encoding == null) {
			System.out.println("EncodingFilter# デフォルトのエンコーディング(UTF-8)を利用します。");
			encoding = DEFAULT_ENCODING;
		} else {
			System.out.println("EncodingFilter# 設定されたエンコーディング(" + encoding
					+ ")を利用します。。");
		}
	}

	@Override
	public void destroy() {
	}

}
